# go-succinctly
Code samples for the book *Go Succinctly*, published by Syncfusion. Visit http://syncfusion.com/resources/techportal/ebooks to register and download your copy for free.